package socket;

public class SocketMessageModel {
    public String type;
    public String topic;
    public String subject;
    public TickerDataModel data;

    public void setType(String type) {
        this.type = type;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setData(TickerDataModel data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public String getTopic() {
        return topic;
    }

    public String getSubject() {
        return subject;
    }

    public TickerDataModel getData() {
        return data;
    }

    public SocketMessageModel(String type, String topic, String subject, TickerDataModel data) {
        this.type = type;
        this.topic = topic;
        this.subject = subject;
        this.data = data;
    }

    public SocketMessageModel() {
    }
}
