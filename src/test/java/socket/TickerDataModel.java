package socket;

public class TickerDataModel {
    public String bestAsk;
    public String bestAskSize;
    public String bestBid;
    public String bestBidSize;
    public String price;
    public String sequence;
    public String size;
    public long time;

    public TickerDataModel(String bestAsk, String bestAskSize, String bestBid, String bestBidSize, String price, String sequence, String size, long time) {
        this.bestAsk = bestAsk;
        this.bestAskSize = bestAskSize;
        this.bestBid = bestBid;
        this.bestBidSize = bestBidSize;
        this.price = price;
        this.sequence = sequence;
        this.size = size;
        this.time = time;
    }

    public TickerDataModel() {
    }

    public void setBestAsk(String bestAsk) {
        this.bestAsk = bestAsk;
    }

    public void setBestAskSize(String bestAskSize) {
        this.bestAskSize = bestAskSize;
    }

    public void setBestBid(String bestBid) {
        this.bestBid = bestBid;
    }

    public void setBestBidSize(String bestBidSize) {
        this.bestBidSize = bestBidSize;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getBestAsk() {
        return bestAsk;
    }

    public String getBestAskSize() {
        return bestAskSize;
    }

    public String getBestBid() {
        return bestBid;
    }

    public String getBestBidSize() {
        return bestBidSize;
    }

    public String getPrice() {
        return price;
    }

    public String getSequence() {
        return sequence;
    }

    public String getSize() {
        return size;
    }

    public long getTime() {
        return time;
    }
}
