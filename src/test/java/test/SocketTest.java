package test;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import socket.SocketContext;
import socket.SocketMessageModel;
import socket.SubscribeModel;
import socket.WebClient;

import java.util.Random;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.executeAsyncJavaScript;
import static io.restassured.RestAssured.given;

public class SocketTest {
    private SocketContext context;

    private String getRandomId() {
        int leftLimit = 97;
        int rightLimit = 122;
        int targetStringLength = 7;
        Random random = new Random();
        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private String getSocketConnectUrl() {
        JsonPath response = given()
                .post("https://api.kucoin.com/api/v1/bullet-public")
                .then().log().body().extract().response().jsonPath();
        String token = response.getString("data.token");
        String socketBaseEndpoint = response.getString("data.instanceServers[0].endpoint");
        return socketBaseEndpoint + "?token=" + token + "&connectId=" + getRandomId();
    }

    @Test
    public void socketKucoin() throws JsonProcessingException {
        SubscribeModel subscribeModel = new SubscribeModel();
        subscribeModel.setId(Math.abs(new Random().nextInt()));
        subscribeModel.setResponse(true);
        subscribeModel.setType("subscribe");
        subscribeModel.setTopic("/market/ticker:BTC-USDT");
        subscribeModel.setPrivateChannel(false);
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(subscribeModel);

        context = new SocketContext();
        context.setTimeOut(10);
        context.setBody(json);
        context.setURI(getSocketConnectUrl());
        WebClient.getInstance().connectToSocket(context);

        String firstNormalMessage = context.getMessageList().stream().filter(x->x.contains("\"type\":\"message\""))
                .findFirst().orElseThrow(()-> new RuntimeException("No normal message found"));
        SocketMessageModel messageOne = objectMapper.readValue(firstNormalMessage, SocketMessageModel.class);

        String lastNormalMessage = context.getMessageList().get(context.getMessageList().size() - 1);
        SocketMessageModel lastMessage = objectMapper.readValue(lastNormalMessage, SocketMessageModel.class);

        Assertions.assertNotEquals(messageOne.getData().getSequence(), lastMessage.getData().getSequence());
    }

    @Test
    public void socketUI_ReceiveText() {
        Configuration.browserSize = "1920x1080";
        Selenide.open("https://www.piesocket.com/websocket-tester");
        String url = "wss://s9057.lon1.piesocket.com/v3/channel_1334?api_key=6GZ2Z1Hrdo2drWWYwxFtP63CnweiRAlwi8TEI0GO&notify_self";
        $x("//input[@id='email']").setValue(url);
        $x("//button[@type='submit']").click();
        String expectedMessage = "test socket message 200";
        context = new SocketContext();
        context.setURI(url);
        context.setBody(expectedMessage);
        context.setExpectedMessage(expectedMessage);
        context.setTimeOut(5);

        WebClient.getInstance().connectToSocket(context);
        $x("//pre[@id='consoleLog']").shouldHave(Condition.partialText(context.getBody()));
    }

    @Test
    public void socketUI_SendText() {
        Configuration.browserSize = "1920x1080";
        Selenide.open("https://www.piesocket.com/websocket-tester");
        String url = "wss://s9057.lon1.piesocket.com/v3/channel_1334?api_key=6GZ2Z1Hrdo2drWWYwxFtP63CnweiRAlwi8TEI0GO&notify_self";
        SelenideElement input = $x("//input[@id='email']");
        SelenideElement button = $x("//button[@type='submit']");
        String expectedMessage = "test socket message 200";

        input.setValue(url);
        button.click();

        Runnable sendUIMessage = new Runnable() {
            @Override
            public void run() {
                input.clear();
                input.sendKeys(expectedMessage);
                button.click();
            }
        };

        context = new SocketContext();
        context.setURI(url);
        context.setExpectedMessage(expectedMessage);
        context.setTimeOut(5);
        context.setRunnable(sendUIMessage);

        WebClient.getInstance().connectToSocket(context);
    }
}
